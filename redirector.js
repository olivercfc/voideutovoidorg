var filter = {
  url:
  [
    {hostContains: 'voidlinux.eu'}
  ]
};

function redirect(details) {
  var url = details.url;
  var newurl = url.replace("voidlinux.eu", "voidlinux.org");
  var updating = browser.tabs.update(details.tabId, {url: newurl});
}

browser.webNavigation.onBeforeNavigate.addListener(redirect, filter);

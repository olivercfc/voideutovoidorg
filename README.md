# voidEUtovoidORG

Firefox extension to redirect requests from voidlinux.eu to voidlinux.org


Icons are from https://material.io/tools/icons/?style=baseline and published under Apache license version 2.0.


Extension is available at https://addons.mozilla.org/addon/voidlinuxeutovoidlinuxorg/ under GPL-3